Script de sauvegarde de base de donnée mysql.

On defini au debut le type de language pour le script avec **#!/bin/bash**

Certaine des varible ne sont pas inscrit sur ce fichier, on fait donc appel a un autre fichier les contenant grace a la commande source.
D'autres variables supplémentaire sont defini.

Nous nous placons tout d'abord dans le repertoire ou sont mis les fichier de sauvegarde de la base de donnée avec **cd $HOME/save**
La commande suivante permet de conserver un certains nombre de fichier de sauvegarde, et supprime automatiquement les sauvegardes ancienne de 5 jours
la commande **find** permet de faire une recherche, la condition **-name** permet d'indiquer le nom du fichier, **-type -f** recherche que les fichiers, **-mtime** défini un temps en jour (ici les fichiers de +5 jours), et enfin avec **-exec rm {} \;** permet d'executer la commande de suppresion sur tout les fichier validant les conditions précédentes.

Nous pouvons ensuite commencer a suavegarder notre base de donnée en fonctions des variable defini dans le fichier backupconf. Nous sauvegardons ici vers le $HOME/save/ de l'utilisateur.
**mysqldump** est une commande permettant de faire des sauvegarde, la condition **-name** et **-password** permet d'entrer un nom d'utilisateur et son mot de passe ayant acces a la database, **-databases** permet de definir la ou les database que l'on souhaite souvegarder. Nous déplacons ensuite la suavegarde vers $HOME/save/ avec le format save$fic.sql .

Pour eviter des erreur, une verification a l'aide de if/fi est faite. Si le resultat de la precedente commande effectuée (mysqldump) est nul, **if [ "$?"=0 ]** alors la sauvegarde est reussi et on envoie le resultat dans de le fichier de log **tee -a "$log"** .
Sinon on redirige le flux vers le flux d'erreur, **1>&2**, et on enregistre ca dans le fichier de log, **tee -a "$log"**.

```
#!/bin/bash

source $HOME/.backupconf

fic=$(date '+%d-%m-%Y')
chemin="$HOME/save"
jour="+5"
nom="save*.sql"
log="/var/log/save/save.log"

cd $HOME/save

find "$chemin" -name "$nom" -type - f -mtime "$jour" -exec rm -f {} \;

mysqldump --user "$user" --password="$mdp" --databases $bdd > "$HOME/save/save$fic.sql"

if [ "$?"=0 ]
	then
		echo "$(date +%d-%m-%Y) sauvegarde faite" | tee -a "$log"
	else
		echo "$(date +%d-%m-%Y) erreur de sauvegarde 1>&2 | tee -a "$log"
		
		exit 1
fi
```