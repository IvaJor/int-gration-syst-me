# Intégration-Système

Documentation sur l'optimisation de mon laptop, et sur mon projet Ynov.

 * [Mon Laptop](https://gitlab.com/IvaJor/int-gration-syst-me/blob/master/S%C3%A9curisation%20Laptop.md)
 * [Mon projet](https://gitlab.com/IvaJor/int-gration-syst-me/blob/master/Projet.md)
 * [Script de sauvegarde Mysql](https://gitlab.com/IvaJor/int-gration-syst-me/blob/master/script%20mysql/script_sauvegarde_mysql.md)