## Description du projet

Il s'agit de réaliser un  serveur de voip. Pour cela je vais utiliser Asterisk sur une base linux debian.

## Installation

Avant de pouvoir le configurer, il nous faut tout d’abord l’installer. Nous allons utiliser ici un Linux Debian en ligne de commande.
Pour installer Asterisk, il suffit d’entrer la commande suivante : 

```
apt-get install asterisk
```
Nous nous dirigerons ensuite dans le dossier asterisk crée automatiquement lors de l’installation :
```
cd /etc/asterisk
``` 

Depuis ce dossier nous avons accès au diffèrent fichier de configuration. Nous allons nous intéresser ici aux principaux soit sip.conf, extensions.conf et voicemail.conf

## Configuration sip.conf

Tout d’abord nous allons éditer le ficher sip.conf afin de crée un contexte ainsi que nos utilisateurs :

Dans les sections master/grp1/grp2, nous donnons la configuration par default, que nous lions aux contextes souhaités.
Tout en incluant les autres, de cette façon ils peuvent communiquer entre eux.

On y attribut les différents codecs voulu (alow, ilbc, gsm, h261, …)

```
[master]
context=master
include=grp1
include=grp2
srvlookup=yes
videosupport=no
disallow=all
allow=alaw
allow=gsm
allow=h323
allow=ilbc

[grp1]
context=grp1
include=master
include=grp2
srvlookup=yes
videosupport=no
disallow=all
allow=alaw
allow=gsm
allow=h323
allow=ilbc

[grp2]
context=grp2
include=mastrer
include=grp1
srvlookup=yes
videosupport=no
disallow=all
allow=alaw
allow=gsm
allow=h323
allow=ilbc
```
Nous créons ensuite les différents utilisateurs, suivant le même schéma, soit :

```
[Master]
type=friend
username=master
secret=123456
qualify=no
nat=no
host=dynamic
canreinvite=no
context=master
callerid="master" <1000>

[user1]
type=friend
username=user1
secret=123456
qualify=no
nat=no
host=dynamic
canreinvite=no
context=grp1
callerip="user1" <1010>

[user2]
type=friend
username=user2
secret=123456
qualify=no
nat=no
host=dynamic
canreinvite=no
context=grp2
callerip="user2" <1020>

```

* Type : ici friend, afin d’autoriser les appels entrants et sortants
* Le nom d’utilisateur ainsi que le mot de passe associé.
* On incris leur contexte associé, en effet nous avons défini groupe de travail.


## Configuration extension.conf

Nos utilisateurs étant créé, il nous faut éditer le fichier extensions.conf afin de réaliser le plan d’appel.
Pour cela, nous créons une règle dans le contexte associe aux utilisateurs crée dans le fichier sip.conf. Ils suivront le format suivant :

```
Exten => « n° du poste », « priorité », « action » (« protocole utilisé »/ « utilisateur associé », « temps avant de passer à la règle suivante »)
```

Nous editons ainsi le fichier:

```
;////////////master\\\\\\\\\\\\

[master]

include => grp1
include => grp2

;menuvocal

exten => 1000,1,Answer()
exten => 1000,2,agi(googletts.agi,"Bonjour et bienvenue sur ce serveur test!",fr)
exten => 1000,3,Hangup()
exten => 1000,4'agi(googletts.agi,"pour joindre L'utilisateur 1 taper 1",fr,any)
exten => 1000,5,agi(googletts.agi,"pour joindre L'utilisateur 2 taper 2",fr,any)
exten => 1000,6,WaitExten()
exten => 1,1,Goto(1010,1)
exten => 2,1,Goto(1020,1)
exten => _[3-9#],1,Goto(1000,3)
exten => t,1,Goto(1000,3)
exten => 1000,7,End

;////////////grp1\\\\\\\\\\\\

[grp1]

include => master
include => grp2

exten => 1010,1,GotoIfTime(08:00-18:00,mon-fry,*,*)
exten => 1010,2,Dial(SIP/user1,20)
exten => 1010,3,Goto(1020,20)
exten => 1010,4,Voicemail(1010@grp1)

;////////////grp2\\\\\\\\\\\\

[grp2]

include => master
include => grp1

exten => 1020,1,GotoIfTime(08:00-18:00,mon-fry,*,*)
exten => 1020,2,Dial(SIP/user2,20)
exten => 1020,3,Goto(1010,20)
exten => 1020,3,Voicemail(1020@grp2)
```

## Configuration voicemail.conf

Nous allons, dans ce fichier, configurer les accès utilisateur a leur boite vocale de messagerie.
Un numéro de messagerie a été définit ainsi qu’un code pin, le tout associé à l’utilisateur.

```
« n° de messagerie » => « code pin », « utilisateur »
```

D’autres options peuvent être ajoutée à la suite, comme un mail sur lequel sera
envoyé le message.

```
[master]
1000 => 1234,master,

[grp1]
1010 => 1234,user1,

[grp2]
1020 => 1234,user2,
``` 

## Application

Notre serveur Asterisk étant configurer, nous allons tester nos utilisateurs via un softphone. 
Pour cela, nous allons installer microsip. Une fois installer, nous ajouter nos utilisateurs comme suis : 
L’adresse du serveur SIP ainsi que du proxy correspond au serveur Asterisk, ici : 

On y indique le nom d’utilisateur, login et mot de passe défini précédemment.
L’adresse du domaine lui aussi correspond au serveur Asterisk.
Puis nous sauvegardons.

Nous faisons de même pour le second utilisateur.

![](image/img5.png) 